const qs = require('querystring')
const _url = require('url')
const checkedList = require('./checkedList.json')
const Telegraph = require('telegra.ph')
const rp = require('request-promise-native')
const { JSDOM } = require('jsdom')

const telegraphClient = new Telegraph()

const isUrl = (url) => {
  return !!url.match(/^http(s?):\/\/\S+/)
}

// const getParserUrl = (url) => {
//   return `https://instantread.keipixel.com/?url=${url}`
// }

// const getIVUrl = (url) => {
//   const search = qs.stringify({
//     rhash: process.env.IV_RHASH,
//     url: getParserUrl(url)
//   })
//   return `https://t.me/iv?${search}`
// }

const isChecked = (url) => {
  const { hostname } = _url.parse(url)
  const domain = hostname.replace(/^www\./, '')
  return checkedList.includes(domain)
}

const domToNode = (domNode) => {
  if (domNode.nodeType == domNode.TEXT_NODE) {
    return domNode.data
  }
  if (domNode.nodeType != domNode.ELEMENT_NODE) {
    return false
  }
  var nodeElement = {}
  nodeElement.tag = domNode.tagName.toLowerCase()
  for (var i = 0; i < domNode.attributes.length; i++) {
    var attr = domNode.attributes[i]
    if (attr.name == 'href' || attr.name == 'src') {
      if (!nodeElement.attrs) {
        nodeElement.attrs = {}
      }
      nodeElement.attrs[attr.name] = attr.value
    }
  }
  if (domNode.childNodes.length > 0) {
    nodeElement.children = []
    for (var i = 0; i < domNode.childNodes.length; i++) {
      var child = domNode.childNodes[i]
      nodeElement.children.push(domToNode(child))
    }
  }
  return nodeElement
}

const webParser = (url) => {
  return rp({
    method: 'GET',
    url: `https://mercury.postlight.com/parser?url=${url}`,
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': process.env.PARSER_TOKEN
    },
    json: true
  })
}

const postTelegraph = (title, content, author, authorUrl, returnContent) => {
  return telegraphClient.createAccount('keipixel').then((account) => {
    telegraphClient.token = account.access_token
    const dom = new JSDOM(content)
    const nodeArray = domToNode(dom.window.document.body).children
    return telegraphClient.createPage(title, nodeArray, author, authorUrl, returnContent)
  })
}

const instantRead = async (url) => {
  const data = await webParser(url)
  const result = await postTelegraph(data.title, data.content, data.domain, data.url, true)
  // console.log(result)
  return result
}

module.exports = {
  isUrl,
  // getParserUrl,
  // getIVUrl,
  webParser,
  postTelegraph,
  isChecked,
  instantRead
}

