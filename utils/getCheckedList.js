const fs = require('fs')
const path = require('path')
const rp = require('request-promise-native')
const url = 'https://instantview.telegram.org/contest'

const outFile = path.join(__dirname, 'checkedList.json')

rp(url)
  .then(source => {
    const matched = source.match(/"\/contest\/([^"]+)\/"/g)
    const list = matched.map(str => str.replace(/^"\/contest\//, '').replace(/\/"$/, ''))
    fs.writeFile(outFile, JSON.stringify(list), () => console.log(`Wrote ${outFile}`))
  })
