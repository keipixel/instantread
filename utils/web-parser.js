const qs = require('querystring')
const rp = require('request-promise-native')
const Entities = require('html-entities').AllHtmlEntities

const entities = new Entities()
const cache = {}

module.exports = async (url) => {
  const uri = `https://mercury.postlight.com/parser?${qs.stringify({ url })}`
  const Entities = require('html-entities').AllHtmlEntities;
  if (cache[url]) {
    return cache[url]
  }
  const option = {
    uri,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'x-api-key': process.env.PARSER_TOKEN,
    },
    json: true
  }
  const data = await rp(option)
  data.content = entities.decode(data.content)
  cache[url] = data
  return data
}
