const path = require('path')
const url = require('url')
const qs = require('querystring')
const Koa = require('koa')
const logger = require('koa-logger')
const render = require('koa-ejs')
const serve = require('koa-static')
const webParser = require('./utils/web-parser')

const isDev = process.env.NODE_ENV !== 'production'
const PORT = 3002
const paths = {
  views: path.join(__dirname, 'views'),
  public:  path.join(__dirname, 'public')
}

const app = new Koa()

app.use(logger())
app.use(serve(paths.public))

render(app, {
  root: paths.views,
  layout: 'layout',
  viewExt: 'ejs',
  cache: false,
  debug: false
})

app.use(async (ctx, next) => {
  const _url = url.parse(ctx.url)
  const query = qs.parse(_url.query)
  if (query.url) {
    const data = await webParser(query.url)
    data.content = data.content.replace(/data-src/g, 'src')
    return await ctx.render('index', data)
  }
  await next()
})

app.use(async ctx => {
  ctx.state.title = 'InstantRead'
  await ctx.render('empty')
})

app.listen(PORT)
console.log(`InstantRead is serving on http://127.0.0.1:${PORT}`)

