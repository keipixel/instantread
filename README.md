# InstantReadBot

[@InstantReadBot](https://t.me/InstantReadBot)

A bot to help Telegram user to read webpages in InstantView Mode.

## Setup

add an config file `./config/env` firstly

```
# for development
export BOT_TOKEN=<BOT_TOKEN>
export PARSER_TOKEN=<PARSER_TOKEN>
export IV_RHASH=<IV_RHASH>
# for deployment
export SERVER_USER=<SERVER_USER>
export SERVER_IP=<SERVER_IP>
export PROJECT_PATH=<PROJECT_PATH>
```

## Development

```
yarn dev
```


## Deployment

```
yarn setup
yarn addenv
yarn deploy
```
