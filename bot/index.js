const { promisify } = require('util')
const fs = require('fs')
const path = require('path')
const Telegraf = require('telegraf')
const { isUrl, webParser, postTelegraph, isChecked, instantRead } = require('../utils')

const bot = new Telegraf(process.env.BOT_TOKEN)
const helpFilePath = path.resolve(__dirname, '../config/help.md')
const readFileAsync = promisify(fs.readFile)

// bot.telegram.sendMessage('InstantReadBot', 'dafsdfads', {
//   parse_mode: 'Markdown'
// })

bot.hears(/\/start|\/help/, ({ from, replyWithMarkdown }) => {
  console.log(from)
  readFileAsync(helpFilePath, 'utf-8')
    .then(helpMd => {
      replyWithMarkdown(helpMd)
    })
})

bot.hears(/(\/read|\/r) .+/, ({ message, replyWithMarkdown }) => {
  const url = message.text.replace(/^(\/read|\/r)\s+/, '')
  if (isUrl(url) && !isChecked(url)) {
    // const ivUrl = getIVUrl(url)
    // const parserUrl = getParserUrl(url)
    console.log(url)
    instantRead(url)
      .then(result => {
        replyWithMarkdown(`[${result.title}](${result.url})`)
      })
  }
})

bot.hears(/^http(s?):\/\/\S+/, ({ message, reply, replyWithMarkdown }) => {
  const url = message.text
  if (isUrl(url) && !isChecked(url)) {
    // const ivUrl = getIVUrl(url)
    // const parserUrl = getParserUrl(url)
    // replyWithMarkdown(`[${parserUrl}](${ivUrl})`)
    console.log(url)
    instantRead(url)
      .then(result => {
        replyWithMarkdown(`[${result.title}](${result.url})`)
      })
  }
})

bot.command('ping', (ctx) => ctx.reply('pong'))
bot.hears('ping', (ctx) => ctx.reply('pong'))

bot.on('sticker', (ctx) => ctx.reply('👍'))

bot.startPolling()
console.log('InstantReadBot is running')
