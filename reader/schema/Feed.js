const mongoose = require('mongoose')
const { Schema } = mongoose

const Feed = Schema({
  title: String,
  feedUrl: String,
  link: String,
  entries: Array
})

module.exports = Feed
