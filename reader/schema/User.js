const mongoose = require('mongoose')
const { Schema } = mongoose

const User = Schema({
  chatId: String,
  feeds: [ { type: Schema.Types.ObjectId, ref: 'Feed' } ]
})

module.exports = User
