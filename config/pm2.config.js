module.exports = {
  apps : {
    name : 'InstantReadBot',
    script : 'bot.js',
    'env': {
      'COMMON_VARIABLE': 'true'
    },
    env_production : {
      'NODE_ENV': 'production'
    }
  },
  deploy : {
    production : {
      user : process.env.SERVER_USER,
      host : process.env.SERVER_IP,
      path : process.env.PROJECT_PATH,
      repo : 'git@gitlab.com:keipixel/instantread.git',
      ref  : 'origin/master',
      'post-deploy' : 'source config/env && yarn && pm2 startOrRestart config/pm2.config.js --env production',
      env: {
        'NODE_ENV': 'production'
      }
    }
  }
}

